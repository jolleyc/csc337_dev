<?php
/*
 * File containing Database functions
 *
 *


 define("DB_HOST", "localhost");
 define("DB_ID", "k11943_Dev");
 define("DB_USER", "k11943_Dev");
 define("DB_PASSWORD", "4d4b296e32f7c"); */
 
 require_once('config.php');
 
function connectDB(){
    $link = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if (!$link) {
      die('Could not connect: ' . mysql_error());
    }
    mysql_select_db(DB_ID) or die(mysql_error());
  }
  
function closeDB(){
    mysql_close($link);
  }
  
function parseUID($uid, $values = false){
	/* this function parses the UID.
	 * IF $values is false, we simply return an array of the exploded uid
	 * IF $values is true, we return the array of the values
	 */
	
	$QR = explode("_", $uid);
	
	if (!$values) {
		return $QR;
	}
		elseif ($values) {
			if($QR[0] == 'B'){
				$table = "building";
				$result = getBuilding(null,$QR[1]);
			}
			elseif($QR[0] == 'R'){
				$table = "room";
				$result = getRoom(null,$QR[1]);
				
			}
			elseif($QR[0] == 'I'){
				$table = "item";
				$result = getItem(null,$QR[1]);
			}
			return $result;
	}
	else { return "Theres been a mistake"; }
	
}
  
function getBuilding($id_building=null, $name=null){
    connectDB();
	if($id_building == null) {
		$result = mysql_query("SELECT * FROM building WHERE building.name = '".$name."'")
			or die("GETBUILDING".mysql_error());
		$building = mysql_fetch_array( $result );
	}
	elseif($name == null) {
		$result = mysql_query("SELECT * FROM building WHERE id_building = '".$id_building."'")
			or die("GETBUILDING".mysql_error());
		$building = mysql_fetch_array( $result );
	}
	else {
		echo "failed to do anything useful";
		/*
		$result = mysql_query("SELECT * FROM building WHERE id_building = ".$id_building." and name = ".$name)
			or die("GETBUILDING".mysql_error());
		$building = mysql_fetch_array( $result );
		*/
	}
	closeDB();
    return $building;
  }
  
function getRoom($id_room=null, $num_room=null){
	connectDB();
	if($id_room == null) {
		$result = mysql_query("SELECT * FROM room WHERE num_room = '".$num_room."'")
			or die("GETRoom".mysql_error());
		$room = mysql_fetch_array( $result );
	}
	elseif($num_room == null) {
		$result = mysql_query("SELECT * FROM room WHERE id_room = '".$id_room."'")
			or die("GETRoom".mysql_error());
		$room = mysql_fetch_array( $result );
	}
	else {
		$result = mysql_query("SELECT * FROM room WHERE id_room = '".$id_room."' and num_room = '".$num_room."'")
			or die("GETRoom".mysql_error());
		$room = mysql_fetch_array( $result );
	}
	closeDB();
    return $room;
  }

function getItem($id_item=null, $num_serial=null, $room_num_room = null){
    //function to get item based on PKs
	//note: unfinished, there should be a better way to ignore null parameters
	connectDB();
	if($id_item == null) {
		// query based on serial (MAIN)
		$result = mysql_query("SELECT * FROM item WHERE num_serial = '".$num_serial."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	elseif($num_serial == null) {
		$result = mysql_query("SELECT * FROM item WHERE id_item = '".$id_item."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	elseif($room_num_room != null) {
		$result = mysql_query("SELECT * FROM item WHERE room_num_room = '".room_num_room."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	else {
		$result = mysql_query("SELECT * FROM item WHERE id_item = '".$id_item."' and num_serial = '".$num_serial."'")
			or die("GETRoom".mysql_error());
		$item = mysql_fetch_array( $result );
	}
	closeDB();
	
	$room = getRoom(null,$item['room_num_room']);
	$building = $room['building_name'];
	$item['building_name'] = $building;
    return $item;
  }

function getPathNames() {
	// function to simply get and return all path names in the db
	$query = "SELECT name FROM path";
	connectDB();
	$result = mysql_query($query)
			or die("getPathNames ".mysql_error());
	closeDB();
	
	$i = 0;
	while ($row = mysql_fetch_assoc($result)) {
		$arr[$i] = $row['name'];
	}
	
	return $arr;
}



function getItemIdent($id_item_Ident=null, $num_serial = null) {
	//There are two spots for the item ident tag:
	//the itemIdent table and  the itemIdent_id_itemIdent in the item table
	
	if($id_item_Ident != null){
		
		$query = "SELECT * FROM item WHERE itemIdent_id_itemIdent = ".$id_item_Ident;
	}
	elseif($num_serial != null) {
		$query = "SELECT itemIdent_id_itemIdent FROM item WHERE num_serial = \"".$num_serial."\""; 
	}
	
	else { echo "Something has gone horribly wrong<br />"; }
	
	connectDB();
	$result = mysql_query($query)
		or die("getItemIdent :".mysql_error());
	$ItemIdent = mysql_fetch_array( $result );
	
	closeDB();
	
	return $ItemIdent;
}

function parseBuilding($buildingIn){
	// List where to begin
	// DD column values:
	// [id_building],[name],[date_built],[description],[notes]
	
	$id_building = $buildingIn['id_building'];
	$name        = $buildingIn['name'];
	$date_built  = $buildingIn['date_built'];
	$description = $buildingIn['description'];
	$notes       = $buildingIn['notes'];
	$full_name   = $buildingIn['full_name'];
	
	$head = "<h1>".$full_name."</h1>";
	$table = "
				<br />
				<table id = \"buildingTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Building ID</th>
					<th scope = \"col\" >Building Name</th>
					<th scope = \"col\" >Build Date</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
				</tr>
				<tr>
					<td scope = \"row\" class = \"spec\">".$name."</td>
					<td>".$full_name."</td>
					<td>".$date_built."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
				</tr>
				</table>";
	
	$building = $head."\n".$table."<br />";
	return $building;
}

function parseRoom($roomIn){
	// Get Rooom
	// Get Item(s) to scan (and what order)
	// Return large string to print in Main
	
	// room table columns
	// [id_room],[num_room],[directions],[description],[notes],[building_id_building],[building_name]
	
	
	$id_room              = $roomIn['id_room'];
	$num_room             = $roomIn['num_room'];
	$directions           = $roomIn['directions'];
	$description          = $roomIn['description'];
	$notes                = $roomIn['notes'];
	$building_id_building = $roomIn['building_id_building'];
	$building_name        = $roomIn['building_name'];
	
	$head = "<h1>".$building_name." : ".$num_room."</h1>";
	$table = "
				<br />
				<table id = \"roomTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Room Number</th>
					<th scope = \"col\" >Directions</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
					<th scope = \"col\" >Building</th>
				</tr>
				<tr>
					<td scope = \"row\" class = \"spec\">".$num_room."</td>
					<td>".$directions."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
					<td>".$building_name."</td>
				</tr>
				</table>";
	
	$room = $head."\n".$table."<br />";
	
	$prompt = updatePrompt($uid);
	
	//Lets list all the other items in the room
	//$items = parseItems($num_room);
	
	$room .= "<br />
			 ".$prompt."";
	          //<h2> Items in Room </h2>";
	
	return $room."<br />";
}

function parseItem($itemIn){
	// function that runs when we scan an item QR code
	// columns:
	// [id_item],[num_serial],[type],[priority],[description],[notes],[condition],[date_manufactured],[inspect_interval],[date_last_inspect],[last_inspect_person],[size],[room_num_room],[itemIdent_id_itemIdent]
	
	$id_item             = $itemIn['id_item'];
	$num_serial          = $itemIn['num_serial'];
	$type                = $itemIn['type'];
	$priority            = $itemIn['priority'];
	$description         = $itemIn['description'];
	$notes               = $itemIn['notes'];
	$condition           = $itemIn['condition'];
	$date_manufactured   = $itemIn['date_manufactured'];
	$inspect_interval    = $itemIn['inspect_interval'];
	$date_last_inspect   = $itemIn['date_last_inspect'];
	$last_inspect_person = $itemIn['last_inspect_person'];
	$size                = $itemIn['size'];
	$room                = $itemIn['room_num_room'];
	$ItemIdent           = $itemIn['itemIdent_id_itemIdent'];
	$building			 = $itemIn['building_name'];
	
	$head = "<h1>".$building." : ".$room."</h1>
				<hr />
				<br />
				<h2>".$type." : ".$num_serial."</h2>";
	
	$table = "
				<br />
				<table id = \"itemsTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Item ID</th>
					<th scope = \"col\" >Serial Number</th>
					<th scope = \"col\" >Type</th>
					<th scope = \"col\" >priority</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
					<th scope = \"col\" >Condition</th>
					<th scope = \"col\" >Last Inspected</th>
				</tr>
				<tr>
					<td scope = \"row\" class = \"spec\">".$id_item."</td>
					<td>".$num_serial."</td>
					<td>".$type."</td>
					<td>".$priority."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
					<td>".$condition."</td>
					<td>".$date_last_inspect."</td>
				</tr>
				</table>";
				
	return $head."<hr />".$table;
}

function parseItems($num_room){
	// function to parse a list of items
	// columns
	// [id_item],[num_serial],[type],[priority],[description],[notes],[condition],[date_manufactured],[inspect_interval],[date_last_inspect],[last_inspect_person],[size],[room_id_room],[room_num_room],[room_building_id_building],[room_building_name]
	
	$table = "
				<br />
				<table id = \"itemsTable\" cellspacing = \"0\" >
				<tr> 
					<th scope = \"col\" >Item ID</th>
					<th scope = \"col\" >Serial Number</th>
					<th scope = \"col\" >Type</th>
					<th scope = \"col\" >priority</th>
					<th scope = \"col\" >Last Inspect Date</th>
					<th scope = \"col\" >Description</th>
					<th scope = \"col\" >Notes</th>
					<th scope = \"col\" >Condition</th>
				</tr>";
				
	connectDB();
	
	$result = mysql_query("SELECT * FROM item WHERE room_num_room = '".$num_room."'")
			or die("GETRoom".mysql_error());
	
	while ($row = mysql_fetch_assoc($result)) {
		$id_item             = $row['id_item'];
		$num_serial          = $row['num_serial'];
		$type                = $row['type'];
		$priority            = $row['priority'];
		$description         = $row['description'];
		$notes               = $row['notes'];
		$condition           = $row['condition'];
		$date_manufactured   = $row['date_manufactured'];
		$inspect_interval    = $row['inspect_interval'];
		$date_last_inspect   = $row['date_last_inspect'];
		$last_inspect_person = $row['last_inspect_person'];
		$size                = $row['size'];
		
		$table .= "
				 <tr>
					<td scope = \"row\" class = \"spec\">".$id_item."</td>
					<td>".$num_serial."</td>
					<td>".$type."</td>
					<td>".$priority."</td>
					<td>".$date_last_inspect."</td>
					<td>".$description."</td>
					<td>".$notes."</td>
					<td>".$condition."</td>
					
				</tr>";
	}
	
	$table .= "</table>";	
	
	closeDB();
				
	return $table."<br />";
}

function parseItemIdent($itemIdentIn) {
	// we are given a room
	
	
	$head = "<h2>Wall Identity for Item in Room:</h1>
			<br />
			";
	$end = "
			";
	$room = getRoom(null,$itemIdentIn['room_num_room']);
	$itemIdentIn['building_name'] = $room['building_name'];
	
	$res = parseItem($itemIdentIn);
	$toRet = $head.$res.$end;
	
	return $toRet;
}

function getNextRoom($uid) {
	/* Function to get the next room after scanning a Building or
	 * Room QR code
	 * returns a echo-able variable
	 */
	 
	$QR = parseUID($uid, false);
	
	$nextRoomPara .= "<div id = \"nextRoom\">
						<p>
							<h2> The Next Room to visit is: </h2>
							";
	
	if($QR[0] == 'B'){
		//we get the first room
		connectDB();
		
		$query = "SELECT * from room WHERE building_name = \"".$QR[1]."\" AND id_room = (SELECT MIN(id_room) FROM room)";
		$result = mysql_query($query)
			or die("GETBUILDING".mysql_error());
		$room = mysql_fetch_array( $result );
		closeDB();
		
		$nextRoomPara .= parseRoom($room);
			
	}
	elseif($QR[0] == 'R'){
		// we get the room with the next id_room
		$room_num = intval($QR[1]);
		$room_num = $room_num + 1;
		
		connectDB();
		
		$query = "SELECT * from room WHERE building_name = \"science\" AND room_num = \"".strval($room_num)."\"";
		$result = mysql_query($query)
			or die("GETBUILDING".mysql_error());
		$room = mysql_fetch_array( $result );
		closeDB();
		
		$nextRoomPara .= parseRoom($room);
	}
	
	$nextRoomPara .= "
					</p>
				</div>
				";
				
	 return $nextRoomPara;
}

function getItemsInRoom($uid) {
	/* function to list all items in a room
	 *
	 *
	 */
	
	$itemsPara = "<div id=\"itemsInRoom\">
					<p>
						";
	
	$QR = parseUID($uid, false);
	
	if ($QR == 'R') {
		connectDB();
		$query = "SELECT * from item WHERE room_num_room = \"".$QR[1]."\"";
		$result = mysql_query($query)
			or die("getItemsInRoom_R".mysql_error());
		$room = mysql_fetch_array( $result );
		closeDB();
		
		$itemsPara .= parseRoom($room);
	}
	elseif ($QR == 'I') {
		//TODO
		
		$itemsPara .= "Please scan the room again";
	}
	
	$itemsPara .= "
				</p>
			</div>
			";
			
	return $itemsPara;
}


function startTourPrompt($uid) {
	/* Simple helper function to display a common prompt
	 *
	 *
	 */
	$prompt = "
	<div id = \"startTourPrompt\">
		<h1>Begin new path?</h1>
		<p>
			<li>This QR code is not the begning of a pre-determined path!</li>
			<br />
			<a href = \"pathing.php\" class = \"button orange\" >Begin New Path </a>
			<a href = \"index.php?onTour=true&uid=".$uid."\"  class = \"button orange\" >Begin Tour Here </a>
			<a href = \"javascript:void(0)\" onclick = \"droid_scan()\" class = \"button orange\" >Scan QR code with Android </a>
		</p>
	</div>
	";
	
	return $prompt;
}

function updatePrompt($uid) {
	/* Helper function
	 * just display a button asking if  information is correct
	 */
	 
	 $prompt = "
				<br />
				<a href = \"recordupdate.php?uid=".$uid."\" class = \"button orange\" > Change these values </a>
				<br />
				";
	return $prompt;
}

function missingPrompt($uid) {
 /* just display a button asking if  Item is missing
	*/
	 
	 $prompt = "
				<br />
				<a href = \"missingItem.php?uid=".$uid."\" class = \"button orange\" > Item is missing </a>
				<br />
				";
	return $prompt;
}

function updateItemInspect($uid) {
/*
 * Update an item's last inspect date
 */
 $QR = parseUID($uid, false);
 
 $query = "UPDATE item SET date_last_inspect = curdate() WHERE num_serial LIKE ".$QR[1];
 connectDB();
 $result = mysql_query($query)
	or die("UPDATEITEMINSPECT".mysql_error());
 closeDB();
 
 return true;
}

function wrongLocationPrompt($uid) {
	/*
	 * This function simply tells us where to place the object. 
	 *
	 */
	$prompt = "
				<br />
				<a href = \"missingItem.php?missing_uid=".$uid."\" class = \"button orange\" > Find Item's Correct Location </a>
				<br />
				";
	return $prompt;
}

function getNext($pathName, $uid) {
	//follow
	$query = "";
	
	if ($uid == null) {
		// if we don't have a UID, we return the first stop
		$query = "SELECT current FROM path where id_path = (SELECT MIN(id_path) FROM path)";
	}
	else {

		//MYSQL query to get the next UID
		$query = "SELECT next FROM path WHERE current = \"".$uid."\"";
	}
	
	connectDB();
	$result = mysql_query($query)
		or die("getNext".mysql_error());
	closeDB();
	$next = mysql_fetch_array($result);
	
	
	if(isset($next['current'])){
		$QR = parseUID($next['current'], false);
	}
	elseif(isset($next['next'])) {
		$QR = parseUID($next['next'], false);
	}
	elseif(!isset($next['next']) and !isset($next['current'])){
		return "
				<h1>You've reached the end of the path, please select a new path to continue</h1>
				<br />
				<br />
				<a href = \"index.php\" class = \"button orange\" > Select a Path </a>";
	}
	
	if($QR[0] == 'B'){
		$result = getBuilding(null,$QR[1]);
		$table = parseBuilding($result);
	}
	elseif($QR[0] == 'R'){
		$result = getRoom(null,$QR[1]);
		$table = parseRoom($result);
		
	}
	elseif($QR[0] == 'I'){
		$result = getItem(null,$QR[1]);
		$table = parseItem($result);
	}
	elseif($QR[0] == 'II') {
		$result = getItemIdent($QR[1],null);
		$table = parseItemIdent($result);
	}
	
	$heading = "<h2>Next QR code to Scan: </h2>
				<br />
				";
	return $heading.$table;
}

?>