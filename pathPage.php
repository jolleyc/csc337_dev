<?php 
/*
 * Page to function as a way of creating new paths
 * test
 * simpliest way: click button initiating a new path
 * scan qr code, each time we insert a new value (uid)
 */
?>

<?php
/*
function update($path, $uid){
	if(success){ return true; }
	elseif(!success) { return false; }
}
*/
?>


<?php
// Load doctype
require_once('common/doctype.html');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<?php 
//htmlHeader HTML
require_once('common/HTMLheader.php');
?>
<body>

<div id = "wrap">
<?php
//Header HTML
require_once('common/header.html');
?>
	<div id = "content">
		<div id = "main" >
			<p>
			To create a new path, two possibilites exist:<br />
			We can have a person going around and scanning qr codes in an order. the path would be set to replicate the path followed once. 
			</p>
			<p>
			Or, we can have a series of drop down boxes.
			</p>
			<p>
			To edit paths, we will be able to draw them in a table and manually edit entries. 
			</p>
			
			<br />
			
			<p>
			<table cellspacing = "0">
				<tr> 
					<th scope = "col" >Order</th>
					<th scope = "col" >Name</th>
					<th scope = "col" >Type</th>
					<th scope = "col" >Room Number</th>
					<th scope = "col" >Description</th>
					<th scope = "col" >Notes</th>
				</tr>
				<tr>
					<td scope = "row" class = "spec">1</td>
					<td>Science</td>
					<td>Building</td>
					<td></td>
					<td>ECSU main campus</td>
					<td></td>
				</tr>
				<tr>
					<td scope = "row" class = "spec">2</td>
					<td></td>
					<td>Room</td>
					<td>101</td>
					<td>First on right</td>
					<td></td>
				</tr>
				<tr>
					<td scope = "row" class = "spec">3</td>
					<td></td>
					<td>Item</td>
					<td>101</td>
					<td>Fire Extinguisher</td>
					<td></td>
				</tr>
				<tr>
					<td scope = "row" class = "spec">4</td>
					<td></td>
					<td>Item</td>
					<td>101</td>
					<td>Fire Extinguisher</td>
					<td></td>
				</tr>
				<tr>
					<td scope = "row" class = "spec">5</td>
					<td></td>
					<td>Room</td>
					<td>102</td>
					<td>Across from 101</td>
					<td></td>
				</tr>
			</table>
			</p>
		</div>
		
		<div id="side">
			<div id="sidea">
				<?php require_once('common/sideA.php'); ?>
			</div>
			<div id="sideb">
				<?php require_once('common/sideB.php'); ?>
			</div>
			<div id="sidec">
				<?php require_once('common/sideC.php'); ?>
			</div>
		</div>
		
	</div> <!-- close content-->
	<?php //load footer
	// load closing files
	require_once('common/footer.html'); 
	?>
	
</div> <!-- close Wrap--->

</body>
</html>