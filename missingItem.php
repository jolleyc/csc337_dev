

<?php
// Load doctype
require_once('common/doctype.html');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
<?php 
//htmlHeader HTML
require_once('common/HTMLheader.php');
?>
<body>

<div id = "wrap">
<?php
//Header HTML
require_once('common/header.html');
?>
	<div id = "content">
		<div id = "main" >
			<p>
			<?php
			/*
			 * missingItem.php
			 * basically, we're just going to enter the 'condition' of an item as 'missing'
			 */
			 
			//get the variables
			$uid = $_GET["uid"]; // UID for missing item
			$missing_uid = $_GET["missing_uid"]; // UID for a found item whose location we need to find 
			$found_uid = $_GET["found_uid"]; // uid for an item replaced in the correct spot
			
			require_once('database.php');
			
			function reportMissing($num_serial) {
			 
				$query = "UPDATE item SET item.condition = \"missing\" WHERE num_serial LIKE \"".$num_serial."\"";
				connectDB();
				$result = mysql_query($query)
					or die("missingItem".mysql_error());
				closeDB();
				
				return "<h1>Thank you for reporting this item missing</h1>
						<hr />
						<br />";
			}
			
			function reportReturned($num_serial) {
			 
				$query = "UPDATE item SET item.condition = \"good\" WHERE num_serial LIKE \"".$num_serial."\"";
				connectDB();
				$result = mysql_query($query)
					or die("missingItem".mysql_error());
				closeDB();
				
				return "<h1>Thank you for Replacing this item</h1>
						<hr />
						<br />";
			}

			if(isset($missing_uid)) {
				// If an item is missing we dispaly where it belongs, annd change 
				$QR = parseUID($missing_uid, false);
				$item = getItem(null,$QR[1],null);
				$prompt = "
							<h1> This item belongs in Room ".$item['room_num_room']."</h1>
							<a href = \"missingItem.php?uid=".$missing_uid."\" class = \"button orange\" >Report Missing</a>  <a href = \"missingItem.php?found_uid=".$missing_uid."\" class = \"button orange\" >Item Returned</a>
							";
				echo $prompt;
			}
			elseif(isset($uid)) {
				// if we're getting a UID then we're getting a wall item identifier
				// so, we're going to get the item responsible
				$QR = parseUID($uid, false);
				
				if($QR[0] == 'I') {
					echo reportMissing($QR[1]);
				}
				elseif($QR[0] == 'II') {
					$item = getItemIdent($QR[1],null);
					echo reportMissing($item['num_serial']);
				}
				else {
					echo "POUR QUEEEEEE?!";
				}
				$next = getNext(null,$uid);
				echo $next;	
			}
			elseif(isset($found_uid)) {
				$QR = parseUID($found_uid, false);
				echo reportReturned($QR[1]);
				$next = getNext(null,$found_uid);
				echo $next;
			}
			

			require_once('common/scan.php');
			$button = drawScanButton();
			echo $button;

			?>

			</p>
		</div>
		
		<div id="side">
			<div id="sidea">
				<?php require_once('common/sideA.php'); ?>
			</div>
			<div id="sideb">
				<?php require_once('common/sideB.php'); ?>
			</div>
			<div id="sidec">
				<?php require_once('common/sideC.php'); ?>
			</div>
		</div>
		
	</div> <!-- close content-->
	<?php //load footer
	// load closing files
	require_once('common/footer.html'); 
	?>
	
</div> <!-- close Wrap--->

</body>
</html>